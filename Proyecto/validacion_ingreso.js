//Simulacion de una base de datos (Lista)
function obtenerListaUsuario(){
    var listaUsuarios = JSON.parse(localStorage.getItem('listaUsuariosLS'));
    
    if(listaUsuarios == null){
        listaUsuarios = 
        [
            ['1','Dolores','doloresmun15@gmail.com','12345','1']
            
        ]
    }
    return listaUsuarios;
}

//Poder ingresar, iniciar cesion
function validarCredenciales(pCorreo , pContraseña){
    var listaUsuarios = obtenerListaUsuario();
    var bAcceso = false;

    for(var i = 0; i < listaUsuarios.length; i++){
        if(pCorreo == listaUsuarios[i][2] && pContraseña == listaUsuarios[i][3]){
            bAcceso = true;
            sessionStorage.setItem('usuarioActivo', listaUsuarios[i][1]);
            sessionStorage.setItem('rolUsuarioActivo', listaUsuarios[i][4]);
        }
    }

    return bAcceso;
}